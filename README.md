# Programação Orientada a Serviços

* **Curso:** [Análise e Desenvolvimento de Sistemas - IFPB Cajazeiras](http://www.ifpb.edu.br/campi/cajazeiras/cursos/cursos-superiores-de-tecnologia/analise-e-desenvolvimento-de-sistemas)
* **Professor:** Ricardo Job
* **Precisando de Ajuda?**
    * Consulte esta página ou crie uma  [issue](https://bitbucket.org/ricardojob/ads-pos/issues).
    * [Email do grupo](mailto:pos-20141@googlegroups.com) para comunicados entre professor e alunos.
	* [Email](mailto:ricardo.job@ifpb.edu.br) para dúvidas pontuais, ou para marcar uma reunião.
* **[Material da Disciplina](https://drive.google.com/folderview?id=0B3bNBHsD1S9gbUhFNmxHMkRReW8) **
* **[Notas da Disciplina](https://docs.google.com/spreadsheets/d/1ME0N9DpmAEToj-2p9MeB_v3aXOUsfGSxJOtG9RjuCOc/edit#gid=295168816)**

## Objetivo Geral

Permitir o aprendizado de conceitos e técnicas fundamentais necessários à Publicação, Descoberta e Integração aplicações orientadas a Serviços.

## Objetivos Específicos

* Entender os fundamentos da Publicação de um serviço Web;
* Entender e aplicar o paradigma de Orientação a Serviços;
* Conhecer e utilizar os principais conceitos de Descoberta e Publicação de Serviços Web.

## Avaliação

* Projeto 1 (Primeira Parte) – 30%
* Projeto 2 (Segunda Parte) – 30%
* Atividade Contínuas – 40%

## Conteúdo Programático

### Semana 1
1. Introdução:
	* Apresentação da disciplina 
	* Definição das atividades, projetos e avaliações.
	* Data: 14/05/2014
2. Introdução aos Web Services
	* Data: 15/05/2014
	* [Aula](https://docs.google.com/file/d/0B3bNBHsD1S9gTWYtelFkN3lldGs/)		
### Semana 2
3. Fundamentos de Web Services
	* Data: 21/05/2014
	* [Aula](https://docs.google.com/file/d/0B3bNBHsD1S9gTWYtelFkN3lldGs/)	
4. Modelos de  Computação Distribuida
	* Data: 22/05/2014
	* [Exemplo em CORBA](https://docs.google.com/file/d/0B3bNBHsD1S9gSjM2Y3BQeF9FQm8/)
	* Atividade para próxima aula: Exemplo simples (e.g. Calculadora) com implementação em CORBA e Java RMI.
	* [Aula](https://docs.google.com/file/d/0B3bNBHsD1S9gMWxYTVY4akFPUTQ/)		
### Semana 3
5. Modelos de  Computação Distribuida
	* Data: 28/05/2014
	* [Aula](https://docs.google.com/file/d/0B3bNBHsD1S9gMWxYTVY4akFPUTQ/)	
6. Web Services
	* Data: 29/05/2014
	* [Aula](https://docs.google.com/file/d/0B3bNBHsD1S9gUWhqdUdZRlFpd1E/)	
### Semana 4
7. Web Services
	* Data: 04/06/2014
	* [Aula](https://docs.google.com/file/d/0B3bNBHsD1S9gUWhqdUdZRlFpd1E/)
8. Tecnologias Web Services
	* Data: 05/06/2014
	* [Aula](https://docs.google.com/file/d/0B3bNBHsD1S9gTXhyMmV0QUlKS2s/)	
### Semana 5
9. Tecnologias Web Services
	* Data: 11/06/2014	
	* [Aula](https://docs.google.com/file/d/0B3bNBHsD1S9gTXhyMmV0QUlKS2s/)
10. UDDI
	* Data: 12/06/2014
	* [Aula](https://docs.google.com/file/d/0B3bNBHsD1S9gN1FvM2ROME5oWWM/)	
### Semana 6
11. JAX-WS
	* Data: 9/07/2014
	* JAX-WS: Definição, aplicabilidade e exemplos.
	* [Aula](https://docs.google.com/file/d/0B3bNBHsD1S9gNnFTVG5pMWhwd0k)
	* [JAX-WS com AXIS](https://docs.google.com/file/d/0B3bNBHsD1S9gRHF0Q2tyTURqTUU/)
	* [Building Web Services with JAX-WS](https://docs.google.com/file/d/0B3bNBHsD1S9gc2hORzFxVmwyN0E/)
	* [Lista de Comandos](https://docs.google.com/file/d/0B3bNBHsD1S9gRVRPMS1DNzlucEE/)
	* [Projeto exemplo](https://bitbucket.org/ricardojob/ads-pos-jaxws/)		
12. JAX-WS - Exemplos
	* Data: 10/07/2014
	* JAX-WS.
	* Atividade com exemplos simples para produzir e consumir serviços com JAX-WS.
	* [Aula](https://docs.google.com/file/d/0B3bNBHsD1S9gNnFTVG5pMWhwd0k)
### Semana 7
13. JAX-WS com JPA
	* Data: 16/07/2014
	* Apresentação das atividades utilizando JAX-WS e JPA. 	
14. Atividade 1
	* Data: 17/07/2014
	* Atividade 1 - Material até Modelos de Computação Distribuida (Semana 3).
### Semana 8
15. JAX-WS com JPA
	* Data: 23/07/2014
	* Apresentação e dúvidas sobre a integração entre JAX-WS e JPA. 
	* [Projeto Exemplo](https://bitbucket.org/ricardojob/ads-pos-jaxwsjpa/)
16. Revisão e Projeto - Parte 1
	* Data: 24/07/2014
	* Revisão sobre JAX-WS, WebServices e Modelos de Computação distribuida.
	* Apresentação do Projeto - Parte 1.
### Semana 9
17. Aula Cancelada
	* Data: 30/07/2014
	* Não houve aula. Motivo: Professor doente
18. Aula Cancelada
	* Data: 31/07/2014
	* Não houve aula. Motivo: Professor doente
### Semana 10
19. REST - Fundamentos
	* Data: 06/08/2014
	* Introdução e apresentação sobre a arquitetura REST
	* [Aula](https://docs.google.com/file/d/0B3bNBHsD1S9gNVpjN2F2Q29hTkU/)
20. REST - Fundamentos
	* Data: 07/08/2014
	* Recursos do REST, REST x RPC e métodos do HTTP 
	* [Aula](https://docs.google.com/file/d/0B3bNBHsD1S9gNVpjN2F2Q29hTkU/)
### Semana 11
21. REST - Exemplo 
	* Data: 13/08/2014
	* SOAP x REST e Exemplo 
	* [Aula](https://docs.google.com/file/d/0B3bNBHsD1S9gNVpjN2F2Q29hTkU/)
	* [Projeto Exemplo]()
22. Atividade 2
	* Data: 14/08/2014
	* Apresentação sobre CORBA e RMI.
### Semana 12
23. REST - Exemplo 
	* Data: 20/08/2014
	* Desenvolvimento de uma aplicação Produtora e consumidora de Serviços REST.
24. REST - Exemplo
	* Data: 21/08/2014
	* Desenvolvimento de uma aplicação Produtora e consumidora de Serviços REST.
	* [Projeto da Disciplina](https://docs.google.com/file/d/0B3bNBHsD1S9gSTdRMGg4dDhubTQ/)
	* [Descritor de Serviços](https://docs.google.com/file/d/0B3bNBHsD1S9gSUNiQ01jOXRybjg/)
	* [Projeto Arquitetural - Aula](https://docs.google.com/file/d/0B3bNBHsD1S9gRlFJSFh3NlBtcUE/)
	* [Projeto Arquitetural - Exemplo](https://docs.google.com/file/d/0B3bNBHsD1S9gdU54N2hSRHUzNGc/)
### Semana 13
23. Atividade 3
	* Data: 27/08/2014
	* Atividade 3 - Material até Tecnologias de Web Services (Semana 5). 
24. Atividade 4
	* Data: 28/08/2014
	* Apresentação do exemplo da estrutura do projeto (JAX-WS com JAX-RS).

	
## Bibliografia
* Abinader, J. A.; Lins, R. D. Web Services em Java. Brasport, 2006.    
* Kalin, M. Web Services: Implementando. Alta Books, 2009.  
* Richardson, L; Ruby, S. RESTful Web Services Web services for the real world. O'Reilly, 2007. 

## Bibliografia Complementar
* Papazoglou, M.  Web Services: Principles and Technology.  Addison-Wesley, 2007.  
* Sampaio, C. SOA e Web Services em Java. Brasport, 2006. 

## Referências Online
* Exemplos WDSL [Ex 1](https://wsgw-teste.min-saude.pt:8000/PEM/RegistoPrescricaoMedicamentos/1.00?WSDL) [Ex 2](http://sciencesoft.at/services/latex?wsdl)
* [Directory of Public SOAP Web Services](http://www.service-repository.com/service/overview/-110292811)
* [UDDI](http://juddi.apache.org/demos.html)